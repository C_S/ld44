﻿using LD44.GameStates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace LD44
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        KeyboardState kbState;
        KeyboardState oKbState;

        GameScreen activeScreen;
        MenuScreen menuScreen;
        PlayScreen playScreen;
        RulesScreen rulesScreen;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 710;
            graphics.PreferredBackBufferHeight = 612;
            graphics.ApplyChanges();
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            menuScreen = new MenuScreen(
                this,
                spriteBatch,
                Content.Load<SpriteFont>("BigFont"),
                Content.Load<Texture2D>("Menu/bg"));
            Components.Add(menuScreen);
            menuScreen.Hide();
            
            playScreen = new PlayScreen(
                this,
                spriteBatch);
            Components.Add(playScreen);
            playScreen.Hide();

            rulesScreen = new RulesScreen(
                this,
                spriteBatch,
                Content.Load<SpriteFont>("Font"),
                Content.Load<Texture2D>("Menu/bg"));
            Components.Add(rulesScreen);
            rulesScreen.Hide();

            activeScreen = menuScreen;
            activeScreen.Show();

            activeScreen = menuScreen;
            activeScreen.Show();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            kbState = Keyboard.GetState();

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            if (activeScreen != menuScreen)
            {
                if (CheckKey(Keys.Back))
                {
                    activeScreen.Hide();
                    activeScreen = menuScreen;
                }
            }

            if (activeScreen == menuScreen)
            {
                if (CheckKey(Keys.Enter))
                {
                    if (menuScreen.SelectedIndex == 0)
                    {
                        activeScreen.Hide();
                        activeScreen = playScreen;
                    }

                    if (menuScreen.SelectedIndex == 1)
                    {
                        activeScreen.Hide();
                        activeScreen = rulesScreen;
                    }

                    if (menuScreen.SelectedIndex == 2)
                        Exit();
                }
            }

            if (activeScreen == rulesScreen)
            {
                if (CheckKey(Keys.N))
                {
                    activeScreen.Hide();
                    activeScreen = playScreen;
                }
            }

            base.Update(gameTime);

            activeScreen.Initialize();
            oKbState = kbState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            base.Draw(gameTime);

            if (activeScreen == playScreen)
                playScreen.Draw(gameTime);

            if (activeScreen == menuScreen)
                menuScreen.Draw(gameTime);

            if (activeScreen == rulesScreen)
                rulesScreen.Draw(gameTime);

            spriteBatch.End();
        }

        private bool CheckKey(Keys theKey)
        {
            return kbState.IsKeyUp(theKey) && oKbState.IsKeyDown(theKey);
        }
        
    }
}
