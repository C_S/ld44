﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.GameStates
{
    class ChoiceScreen : DrawableGameComponent
    {
        Texture2D imgBtn;
        Rectangle imgRectBtn;
        SpriteBatch sb;
        SpriteFont sf;
        Vector2 pos;
        List<string> toPrint;
        KeyboardState kbState;
        KeyboardState oKbState;

        public ChoiceScreen(Game game, List<string> toPrint, SpriteBatch sb, SpriteFont sf, Texture2D imgBtn) : base(game)
        {
            this.sb = sb;
            this.sf = sf;

            this.pos = new Vector2(60, 141);
            this.toPrint = toPrint;

            this.imgBtn = imgBtn;
            this.imgRectBtn = new Rectangle((int)pos.X, (int)pos.Y,
                400,
                170);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            sb.Draw(imgBtn, imgRectBtn, Color.White * 0.5f);
            sb.DrawString(sf, ChoiceComposeText(toPrint),new Vector2(70, 151), Color.MonoGameOrange);
            base.Draw(gameTime);
        }

        public string ComposeText(List<string> toPrint)
        {
            StringBuilder strBuild = new StringBuilder();

            string title = char.ToUpper(toPrint[0][0]) + toPrint[0].Substring(1);
            toPrint[0] = title;
            strBuild.AppendLine();
            strBuild.AppendLine();

            if (toPrint.Count > 1)
            {
                foreach (string str in toPrint.Skip(1))
                {
                    strBuild.Append(str);

                    if (!str.Equals(toPrint[toPrint.Count - 1]))
                        strBuild.AppendLine();
                }

                return strBuild.ToString();
            }
            return strBuild.Append("NOTHING !").ToString();
        }

        private string ChoiceComposeText(List<string> toPrint)
        {
            StringBuilder strBuild = new StringBuilder();

            if (toPrint.Count > 1)
            {
                strBuild.Append("Choose something to remove in the ");
                
                foreach (string str in toPrint)
                {
                    strBuild.Append(str);

                    if (!str.Equals(toPrint[toPrint.Count-1]))
                        strBuild.AppendLine();
                }

                return strBuild.ToString();
            }

            return "No choices, press Space to continue.";
        }

        private bool CheckKey(Keys theKey)
        {
            return kbState.IsKeyUp(theKey) && oKbState.IsKeyDown(theKey);
        }
    }
}
