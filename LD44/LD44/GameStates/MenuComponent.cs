﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.GameStates
{
    class MenuComponent : DrawableGameComponent
    {
        #region attributs
        string[] menuItems;
        int selectedIndex;

        Color normal = Color.Black;
        Color hilight = Color.MonoGameOrange;

        Texture2D texture;

        KeyboardState kbState;
        KeyboardState oKbState;

        SpriteBatch sb;
        SpriteFont sf;

        Vector2 pos;
        float width = 0;
        float height = 0f;
        #endregion

        public MenuComponent(Game game, SpriteBatch sb, SpriteFont sf, string[] menuItems, Texture2D texture) : base(game)
        {
            this.sb = sb;
            this.sf = sf;
            this.menuItems = menuItems;
            this.texture = texture;
            MeasureMenu();

            //Débute le menu à 15-15 du bord
            pos.X = Game.Window.ClientBounds.Width/2 - texture.Width/2;
            pos.Y = 150;
        }

        private void MeasureMenu()
        {
            height = 0;
            width = texture.Width;

            foreach (string item in menuItems)
            {
                Vector2 size = sf.MeasureString(item);

                if (size.X > width)
                    width = size.X;

                if (size.Y > height)
                    height = size.Y;

                if (menuItems.Count() > 1)
                    height += texture.Height + 50;
            }

            height -= 50;
        }

        public int SelectedIndex
        {
            get { return selectedIndex; }

            set
            {
                selectedIndex = value;

                if (selectedIndex < 0)
                    selectedIndex = 0;
                if (selectedIndex >= menuItems.Length)
                    selectedIndex = menuItems.Length - 1;
            }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        private bool CheckKey(Keys theKey)
        {
            return kbState.IsKeyUp(theKey) && oKbState.IsKeyDown(theKey);
        }

        public override void Update(GameTime gameTime)
        {
            kbState = Keyboard.GetState();
            Vector2 menuPos = pos;

            Rectangle buttonRect;

            for (int i = 0; i < menuItems.Count(); i++)
            {
                buttonRect = new Rectangle((int)menuPos.X, (int)menuPos.Y, texture.Width, texture.Height);
                menuPos.Y += texture.Height + 65;
            }

            if (CheckKey(Keys.Down))
            {
                selectedIndex++;
                if (selectedIndex == menuItems.Length)
                    selectedIndex = 0;
            }

            if (CheckKey(Keys.Up))
            {
                selectedIndex--;
                if (selectedIndex < 0)
                    selectedIndex = menuItems.Length - 1;
            }

            base.Update(gameTime);
            oKbState = kbState;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            Vector2 menuPos = pos;
            Color tint;

            for (int i = 0; i < menuItems.Length; i++)
            {
                if (i == selectedIndex)
                    tint = hilight;
                else
                    tint = normal;

                sb.Draw(texture, menuPos, Color.White);

                Vector2 textSize = sf.MeasureString(menuItems[i]);

                Vector2 textPos = menuPos + new Vector2((int)(texture.Width - textSize.X) / 2, (int)(texture.Height - textSize.Y) / 2);

                sb.DrawString(
                    sf,
                    menuItems[i],
                    textPos,
                    tint);

                menuPos.Y += texture.Height + 50;
            }
        }
    }
}
