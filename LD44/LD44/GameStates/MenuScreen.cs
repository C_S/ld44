﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.GameStates
{
    class MenuScreen : GameScreen
    {
        MenuComponent menuComponent;
        Texture2D imgBtn;
        Rectangle imgRectBtn;
        SpriteFont sf;
        
        public MenuScreen(Game game, SpriteBatch sb, SpriteFont sf, Texture2D imgBtn) : base(game, sb)
        {
            string[] menuItems = { "Play", "Rules", "Quit" };
            menuComponent = new MenuComponent(game,
                sb,
                sf,
                menuItems,
                game.Content.Load<Texture2D>("Menu/button"));
            Components.Add(menuComponent);
            this.imgBtn = imgBtn;
            this.sf = sf;
            this.imgRectBtn = new Rectangle(0, 0,
                Game.Window.ClientBounds.Width,
                Game.Window.ClientBounds.Height);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Initialize()
        {
            //string[] menuItems = { "Play", "Rules", "Quit" };
            //menuComponent = new MenuComponent(game,
            //sb,
            //sf,
            //menuItems,
            //game.Content.Load<Texture2D>("Menu/button"));
            //Components.Add(menuComponent);
        
            base.Initialize();
        }

        public override void Draw(GameTime gameTime)
        {
            sb.Draw(imgBtn, imgRectBtn, Color.White);
            sb.DrawString(Game.Content.Load<SpriteFont>("FontTitle"), "How will you end your LD ?", new Vector2(150, 30), Color.Black);
            base.Draw(gameTime);
        }

        public int SelectedIndex
        {
            get { return menuComponent.SelectedIndex; }
            set { menuComponent.SelectedIndex = value; }
        }
    }
}
