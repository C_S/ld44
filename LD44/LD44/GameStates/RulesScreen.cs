﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.GameStates
{
    class RulesScreen : GameScreen
    {
        Texture2D img;
        Rectangle imgRect;
        SpriteFont sf;
        Dictionary<string, string> itemsId;
        PlayScreen playScreen;
        KeyboardState kbState;
        KeyboardState oKbState;

        public RulesScreen(Game game, SpriteBatch sb, SpriteFont sf, Texture2D img) : base(game, sb)
        {
            this.sf = sf;
            this.img = img;
            this.imgRect = new Rectangle(0, 0, 
                Game.Window.ClientBounds.Width, 
                Game.Window.ClientBounds.Height);

            itemsId = new Dictionary<string, string>
            {
                {"Press T", "Thurst"},
                {"Press H", "Hunger"},
                {"Press C", "Confidence"},
                {"Press S", "Sanity"},
                {"Press Z", "Sleep"},
                {"Press P", "Hygiene"}
            };

            playScreen = new PlayScreen(
                game,
                sb); ;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sb.Draw(img, imgRect, Color.White);

            Vector2 posData = new Vector2(15, 175);
            int cpt = 0;

            foreach (KeyValuePair<string, string> id in itemsId)
            {
                posData.Y += 40;
                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), String.Format("{0} - {1}", id.Key, id.Value), posData, Color.Black);
                cpt++;
            }

            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Press Space to roll the dice", new Vector2(15, 460), Color.Black);

            StringBuilder strBuild = new StringBuilder();
            strBuild.Append("Hi !");
            strBuild.AppendLine();
            strBuild.Append("You want participate to Ludum Dare ? Fine. Go.");
            strBuild.AppendLine();
            strBuild.Append("...");
            strBuild.AppendLine();
            strBuild.Append("...");
            strBuild.AppendLine();
            strBuild.AppendLine();
            strBuild.AppendLine();
            strBuild.Append("But... How much elements of your life will you lose ?");
            strBuild.AppendLine();

            Vector2 posStory = new Vector2(15, 15);

            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), strBuild.ToString(), posStory, Color.Black);

            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Press N to play.", new Vector2(500, 550), Color.Black);

            sb.Draw(Game.Content.Load<Texture2D>("Items/penalty"), new Rectangle(250, 225, 76, 76), Color.White);
            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Press the right key to choose your penalty", new Vector2(346, 260), Color.Black);

            sb.Draw(Game.Content.Load<Texture2D>("Items/earning"), new Rectangle(250, 310, 76, 76), Color.White);
            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Press the right key to choose your earning", new Vector2(346, 345), Color.Black);

            sb.Draw(Game.Content.Load<Texture2D>("Board/way"), new Rectangle(250, 413, 76, 30), Color.White);
            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Show the way", new Vector2(346, 420), Color.Black);

            sb.Draw(Game.Content.Load<Texture2D>("Board/goodluck"), new Rectangle(100, 500, 332, 56), Color.White);

        }

        private bool CheckKey(Keys theKey)
        {
            return kbState.IsKeyUp(theKey) && oKbState.IsKeyDown(theKey);
        }
    }
}
