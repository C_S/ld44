﻿using LD44.Playables;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.GameStates
{
    class PlayScreen : GameScreen
    {
        Board board;
        public List<Song> songs;

        public PlayScreen(Game game, SpriteBatch sb) : base(game, sb)
        {
            board = new Board(game, sb, game.Content.Load<Texture2D>("Board/boardBG"), Game.Content.Load<Texture2D>("Board/boardData"));
            songs = new List<Song>();
        }

        public override void Initialize()
        {
            base.Initialize();
            board.Initialize();
        }

        protected override void LoadContent()
        {
            this.songs.Add(game.Content.Load<Song>("Song/continueSong"));
            this.songs.Add(game.Content.Load<Song>("Song/finalSong"));

            MediaPlayer.Play(songs[0]);

            MediaPlayer.IsRepeating = true;

            MediaPlayer.Volume = 0.1f;
            
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            board.Draw(gameTime);
            base.Draw(gameTime);
        }


    }
}
