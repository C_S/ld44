﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.Playables
{
    class Dice : DrawableGameComponent
    {
        Random rdm;
        SpriteBatch sb;
        int result = 0;
        bool firstIteration = true;

        public Dice(Game game, SpriteBatch sb) : base(game)
        {
            rdm = new Random();
            this.sb = sb;
            this.result = rdm.Next(1, 7);
        }

        public int Result
        {
            get { return result; }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            if(!firstIteration)
                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), String.Format("+ {0} cases", Result), new Vector2(560, 65), Color.Black);
        }

        public void playDice()
        {
            this.result = rdm.Next(1, 7);
            firstIteration = false;
        }

    }
}
