﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.Playables
{
    public abstract class Item : DrawableGameComponent
    {
        protected Game game;
        protected SpriteBatch sb;
        protected Texture2D text;
        protected Rectangle rectText;
        protected Vector2 size;
        protected Vector2 pos;

        public Item(Game game, SpriteBatch sb, Texture2D text, Rectangle rectText, Vector2 size, Vector2 pos) : base(game)
        {
            this.game = game;
            this.sb = sb;
            this.text = text;
            this.rectText = rectText;
            this.size = size;
            this.pos = pos;
        }

        public Vector2 Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sb.Draw(text, rectText, Color.White);
        }

    }
}
