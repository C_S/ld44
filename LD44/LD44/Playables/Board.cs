﻿using LD44.GameStates;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.Playables
{
    class Board : DrawableGameComponent
    {
        private Vector2 size;
        private Texture2D bg;
        private Rectangle rectBg;
        private Texture2D bgData;
        private Rectangle rectData;
        private Game game;
        private SpriteBatch sb;
        private Random rdm = new Random();
        public Dice dice;
        public Player player;
        
        private KeyboardState kbState;
        private KeyboardState oKbState;

        //Les items du plateau
        int nbItems = 20;
        private List<Penalty> penaltyItems;
        private List<Earning> earningItems;

        //Les vies du joueur
        private List<string> penalties;
        private List<string> earnings;
        private Dictionary<string, string> itemsId;

        ChoiceScreen choiceScreen;
        bool drawChoice = false;
        bool endGame = false;

        //Board
        public Dictionary<int, Vector2> completeBoard;

        public Board(Game game, SpriteBatch sb, Texture2D textBg, Texture2D bgData) : base(game)
        {
            this.size = new Vector2(510, 612);
            this.sb = sb;
            this.bg = textBg;
            this.rectBg = new Rectangle(0, 0, 510, 612);
            this.rectData = new Rectangle(510, 0, 200, 612);
            this.bgData = bgData;
            this.dice = new Dice(game, sb);
            
            this.player = new Player(game, sb, Game.Content.Load<Texture2D>("Player/playerLeft"));

            penalties = new List<string>() {"penalties"};
            earnings = new List<string>() {"earnings", "Thirst", "Hunger", "Sanity", "Sleep", "Confidence", "Hygiene"};
            itemsId = new Dictionary<string, string>
            {
                {"T", "Thurst"},
                {"H", "Hunger"},
                {"C", "Confidence"},
                {"S", "Sanity"},
                {"Z", "Sleep"},
                {"P", "Hygiene"}
            };

            penaltyItems = new List<Penalty>();
            earningItems = new List<Earning>();

            #region Items
            for (int i = 0; i < nbItems; i++)
            {
                int xRdm = rdm.Next(0, 5);
                int yRdm = rdm.Next(0, 6);

                Vector2 size = new Vector2(76, 76);
                Vector2 pos = new Vector2(xRdm, yRdm);

                Item item;
                Rectangle rectTextItem;
                Texture2D textItem;

                if (i % 2 == 0)
                {
                    textItem = game.Content.Load<Texture2D>("Items/penalty");

                    if (penaltyItems.Count() < 1 && earningItems.Count() < 1)
                    {
                        rectTextItem = new Rectangle((int)ConvertPosToPixels(pos).X, (int)ConvertPosToPixels(pos).Y, (int)size.X, (int)size.Y);
                        item = new Penalty(game, sb, textItem, rectTextItem, size, pos);

                        penaltyItems.Add((Penalty)item);
                    }
                    else
                    {
                        if (!penaltyItems.Exists(p => p.Pos == pos) && !earningItems.Exists(e => e.Pos == pos))
                        {
                            rectTextItem = new Rectangle((int)ConvertPosToPixels(pos).X, (int)ConvertPosToPixels(pos).Y, (int)size.X, (int)size.Y);
                            item = new Penalty(game, sb, textItem, rectTextItem, size, pos);

                            penaltyItems.Add((Penalty)item);
                        }
                        else
                            i--;
                    }
                }
                else
                {
                    textItem = game.Content.Load<Texture2D>("Items/earning");

                    if (penaltyItems.Count() < 1 && earningItems.Count() < 1)
                    {
                        rectTextItem = new Rectangle((int)ConvertPosToPixels(pos).X, (int)ConvertPosToPixels(pos).Y, (int)size.X, (int)size.Y);
                        item = new Earning(game, sb, textItem, rectTextItem, size, pos);

                        earningItems.Add((Earning)item);
                    }
                    else
                    {
                        if (!penaltyItems.Exists(p => p.Pos == pos) && !earningItems.Exists(e => e.Pos == pos))
                        {
                            rectTextItem = new Rectangle((int)ConvertPosToPixels(pos).X, (int)ConvertPosToPixels(pos).Y, (int)size.X, (int)size.Y);
                            item = new Earning(game, sb, textItem, rectTextItem, size, pos);

                            earningItems.Add((Earning)item);
                        }
                        else
                            i--;
                    }
                }
            }
            #endregion

            #region completeBoard
            completeBoard = new Dictionary<int, Vector2>
            {
                { 1, new Vector2(4,5)},
                { 2, new Vector2(3,5)},
                { 3, new Vector2(2,5)},
                { 4, new Vector2(1,5)},
                { 5, new Vector2(0,5)},
                { 6, new Vector2(0,4)},
                { 7, new Vector2(1,4)},
                { 8, new Vector2(2,4)},
                { 9, new Vector2(3,4)},
                { 10, new Vector2(4,4)},
                { 11, new Vector2(4,3)},
                { 12, new Vector2(3,3)},
                { 13, new Vector2(2,3)},
                { 14, new Vector2(1,3)},
                { 15, new Vector2(0,3)},
                { 16, new Vector2(0,2)},
                { 17, new Vector2(1,2)},
                { 18, new Vector2(2,2)},
                { 19, new Vector2(3,2)},
                { 20, new Vector2(4,2)},
                { 21, new Vector2(4,1)},
                { 22, new Vector2(3,1)},
                { 23, new Vector2(2,1)},
                { 24, new Vector2(1,1)},
                { 25, new Vector2(0,1)},
                { 26, new Vector2(0,0)},
                { 27, new Vector2(1,0)},
                { 28, new Vector2(2,0)},
                { 29, new Vector2(3,0)},
                { 30, new Vector2(4,0)}
            };
            #endregion
        }

        public override void Initialize()
        {
            kbState = Keyboard.GetState();

            if (CheckKey(Keys.Space))
            {
                dice.playDice();
                player.MovePlayer(dice.Result, player.Pos);
            }

            #region Penalties and earnings gestion
            if (earningItems.Exists(e => (e.Pos == player.Pos)))
            {
                choiceScreen = new ChoiceScreen(game, penalties, sb, Game.Content.Load<SpriteFont>("Font"), Game.Content.Load<Texture2D>("Player/button"));
                drawChoice = true;
                if (CheckKey(Keys.T))
                {
                    AddEarning("Thirst");
                }
                if (CheckKey(Keys.H))
                {
                    AddEarning("Hunger");
                }
                if (CheckKey(Keys.C))
                {
                    AddEarning("Confidence");
                }
                if (CheckKey(Keys.S))
                {
                    AddEarning("Sanity");
                }
                if (CheckKey(Keys.Z))
                {
                    AddEarning("Sleep");
                }
                if (CheckKey(Keys.P))
                {
                    AddEarning("Hygiene");
                }
            }
            else if (penaltyItems.Exists(e => (e.Pos == player.Pos)))
            {
                choiceScreen = new ChoiceScreen(game, earnings, sb, Game.Content.Load<SpriteFont>("Font"), Game.Content.Load<Texture2D>("Player/button"));
                drawChoice = true;
                if (CheckKey(Keys.T))
                {
                    AddPenalty("Thirst");
                }
                if (CheckKey(Keys.H))
                {
                    AddPenalty("Hunger");
                }
                if (CheckKey(Keys.C))
                {
                    AddPenalty("Confidence");
                }
                if (CheckKey(Keys.S))
                {
                    AddPenalty("Sanity");
                }
                if (CheckKey(Keys.Z))
                {
                    AddPenalty("Sleep");
                }
                if (CheckKey(Keys.P))
                {
                    AddPenalty("Hygiene");
                }
            }
            #endregion

            if (player.Pos == new Vector2(4, 0))
                endGame = true;

                base.Initialize();

            oKbState = kbState;
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            sb.Draw(bg, rectBg, Color.White);
            sb.Draw(bgData, rectData, Color.White);

            sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Press Space to roll\n the dice :", new Vector2(520, 15), Color.Black);

            Vector2 posData = new Vector2(550, 100);
            int cpt = 0;

            foreach (KeyValuePair<string, string> id in itemsId)
            {
                posData.Y += 50;
                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), String.Format("{0} - {1}", id.Key, id.Value) , posData, Color.Black);
                cpt++;
            }

            foreach (Penalty penal in penaltyItems)
                penal.Draw(gameTime);
            foreach (Earning earning in earningItems)
                earning.Draw(gameTime);

            dice.Draw(gameTime);

            if(player.Pos.Y%2 == 0)
                player.Text = Game.Content.Load<Texture2D>("Player/playerRight");
            else
                player.Text = Game.Content.Load<Texture2D>("Player/playerLeft");

            player.Draw(gameTime);

            if (drawChoice)
            {
                choiceScreen.Draw(gameTime);
                drawChoice = false;
            }

            if (endGame)
            {
                Vector2 posFinalFirst = new Vector2(100, 100);
                Vector2 posFinalSec = new Vector2(175, 175);
                Vector2 posFinalThird = new Vector2(350, 350);
                Rectangle rectFinal = new Rectangle(0, 0,
                Game.Window.ClientBounds.Width,
                Game.Window.ClientBounds.Width);

                if (penalties.Count > 4)
                {
                    sb.Draw(Game.Content.Load<Texture2D>("EndScreen/bgBad"), rectFinal, Color.White);
                    sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Too bad... You are in terrible condition...", posFinalFirst, Color.Black);
                }
                else
                {
                    sb.Draw(Game.Content.Load<Texture2D>("EndScreen/bgGood"), rectFinal, Color.White);
                    sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "Yeah ! You are in good condition !", posFinalFirst, Color.Black);
                }

                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "What you keep in the end :", posFinalSec, Color.Black);
                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), choiceScreen.ComposeText(earnings), posFinalSec, Color.Black);

                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), "What you lose in the end :", posFinalThird, Color.Black);
                sb.DrawString(Game.Content.Load<SpriteFont>("Font"), choiceScreen.ComposeText(penalties), posFinalThird, Color.Black);
            }

        }

        private Vector2 ConvertPosToPixels(Vector2 pos)
        {
            Vector2 pixels = new Vector2(pos.X * 100 + 12, pos.Y * 100 + 12);

            return pixels;
        }
        
        private bool CheckKey(Keys theKey)
        {
            return kbState.IsKeyUp(theKey) && oKbState.IsKeyDown(theKey);
        }

        #region Méthodes Earning-Penalty

        private void AddEarning(string toAdd)
        {
            if (!earnings.Contains(toAdd))
            {
                earnings.Add(toAdd);
                penalties.Remove(toAdd);
            }
        }

        private void AddPenalty(string toRemove)
        {
            if (!penalties.Contains(toRemove))
            {
                penalties.Add(toRemove);
                earnings.Remove(toRemove);
            }
        }

        #endregion
    }
}
