﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.Playables
{
    class Penalty : Item
    {

        public Penalty(Game game, SpriteBatch sb, Texture2D text, Rectangle rectText, Vector2 size, Vector2 pos) : base(game, sb, text, rectText, size, pos)
        {

        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

    }
}
