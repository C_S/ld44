﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LD44.Playables
{
    class Player : DrawableGameComponent
    {
        Vector2 pos;
        Texture2D text;
        Rectangle textRect;
        SpriteBatch sb;
        
        public Player(Game game, SpriteBatch sb, Texture2D text) : base(game)
        {
            this.Text = text;
            this.sb = sb;
            this.Pos = new Vector2(4, 5);
            this.textRect = new Rectangle((int)ConvertPosToPixels(pos).X, (int)ConvertPosToPixels(pos).Y, 76, 62);
        }
        

        public Vector2 Pos
        {
            get { return pos; }
            set { pos = value; }
        }

        public Texture2D Text
        {
            get { return text; }

            set
            {
                text = value;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            sb.Draw(Text, textRect, Color.White );
        }

        private Vector2 ConvertPosToPixels(Vector2 pos)
        {
            Vector2 pixels = new Vector2(pos.X * 100 + 12, pos.Y * 100 + 19);

            return pixels;
        }

        public void MovePlayer(int nbDice, Vector2 newPos)
        {
            if (nbDice > 0)
            {
                if (newPos.Y % 2 == 1)
                {
                    if (newPos.X == 0)
                        newPos.Y -= 1;
                    else
                        newPos.X -= 1;

                    MovePlayer(nbDice - 1, newPos);
                }
                else if (newPos.Y % 2 == 0)
                {
                    if (newPos.X == 4)
                    {
                        if(!(newPos.Y == 0))
                            newPos.Y -= 1;
                    }
                    else
                        newPos.X += 1;

                    MovePlayer(nbDice - 1, newPos);
                }
            }
            else
            {
                this.textRect.X = (int)ConvertPosToPixels(newPos).X;
                this.textRect.Y = (int)ConvertPosToPixels(newPos).Y;
                this.Pos = newPos;
            }
        }
    }
}
